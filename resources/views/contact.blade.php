@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height">
   <div class="container">
        <h1>Contactanos</h1>
   </div>

    <form method="post" action="/contact/submit">
        @csrf
        <div class="row">

            <div class="container">
                <div class="col-md-8 col-lg-8">
                    <label>Nombre:</label>
                    <input type="text" name="nombre" class="form-control">
                </div>

                <div class="col-md-8 col-lg-8">
                    <label>Correo:</label>
                    <input type="email" name="email" class="form-control">
                </div>

                <div class="col-md-8 col-lg-8">
                    <label>Asunto:</label>
                    <input type="text" name="asunto" class="form-control">
                </div>

                <div class="col-md-8 col-lg-8">
                    <label>Mensaje:</label>
                    <textarea name="mensaje" rows="3" class="form-control"></textarea>
                </div>

                <div class="col-md-8 col-lg-8">
                    <br>
                    <input type="submit" value="Contactar" class="form-control">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
