@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <span class="float-right"><a href="/listings/create" class="btn btn-secondary">Crear listado</a></span></div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Haz iniciado session ') }}

                    <h3>Your Listings</h3>
                    @if (count($listings))
                    <table>
                        <tr>
                            <th>Compañia</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($listings as $listing)
                        <tr>
                            <td>{{$listing->name}}</td>
                            <td><a href="/listings/{{ $listing->id }}/edit" class="btn btn-info float-right">Edit</a></td>
                            <td>
                                <form class="float-right m1-2" action="/listings/{{ $listing->id }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" name="button" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                        @else
                            <p>No tienes algun listado aun</p>
                        @endif
                </div>
            </div>
        </div>
    </div>

@endsection
