@extends('layouts.app')

@section('content')

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('Create')}}

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{session('status')}}
                                </div>
                            @endif

                        </div>

                        <form method="post" action="/listings">
                            @csrf
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Ingresar Nombre">
                            </div>

                            <div class="form-group">
                                <label for="addres">Direccion</label>
                                <input type="text" class="form-control" name="addres" id="addres" placeholder="Ingresar Direccion">
                            </div>

                            <div class="form-group">
                                <label for="Website">Sitio Web</label>
                                <input type="text" class="form-control" name="website" id="website" placeholder="Ingresar Pagina Web">
                            </div>

                            <div class="form-group">
                                <label for="Email">Correo Electronico</label>
                                <input type="text" class="form-control" name="email" id="email" placeholder="Ingresar Correo">
                            </div>

                            <div class="form-group">
                                <label for="Phone">Telefono</label>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="Ingresar Telefono">
                            </div>

                            <div class="form-group">
                                <label for="Bio">Bio</label>
                                <input type="text" class="form-control" name="bio" id="bio" placeholder="Ingresar Bio">
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
@endsection
