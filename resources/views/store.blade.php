<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Proyecto</title>
    <link rel="stylesheet" href="/css/app.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>

@extends('layouts.app')

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Tienda</h1>
        <p class="lead">Hay nuevos diseños cada mes</p>
    </div>

    <div class="container">
        <div class="card-deck mb-3 text-center">
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Basico</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title"> $100 <small class="text-muted"> pesos </small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>3 Diseños</li>
                        <li>1 Diseños personalizados</li>
                        <li>Sin sugerencias de diseñador</li>
                        <li>1 meses de entrega</li>

                    </ul>
                    <button type="button" class="btn btn-lg btn-block btn-primary">Comprar</button>
                </div>
            </div>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal"> Grupal</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">$300 <small class="text-muted"> pesos</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>5 diseños</li>
                        <li>2 personalizados</li>
                        <li>Sugerencias de diseñador</li>
                        <li>3 semanas de entrega</li>
                    </ul>
                    <button type="button" class="btn btn-lg btn-block btn-primary">Get started</button>
                </div>
            </div>
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">Social</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">$500 <small class="text-muted"> pesos</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>10 diseños</li>
                        <li>5 personalizados</li>
                        <li>Ayuda personalizada de diseñador</li>
                        <li>1 semana de entrega</li>
                    </ul>
                    <button type="button" class="btn btn-lg btn-block btn-primary">Comprar</button>
                </div>
            </div>
        </div>
    </div>
</body>
